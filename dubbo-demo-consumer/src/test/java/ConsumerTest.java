import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.bowen.service.ProviderService;

public class ConsumerTest {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"dubbo-demo-consumer.xml"});
        context.start();
        ProviderService demoProviderService=(ProviderService) context.getBean("demoProviderService"); //多态方式
        String result=demoProviderService.sayHello("你好"); //远程调用
        System.out.println("远程调用的结果："+result);
        try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
        context.close();
	}
}
