package com.bowen.service;

/**
 * Created by zhangbw on 2019/1/23
 * Describe：服务提供者接口
 */
public interface ProviderService {
    public String sayHello(String name);
}
