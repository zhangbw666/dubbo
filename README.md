# Dubbo RPC 框架学习笔记

[猿码天地的公众号](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-wechat/%E5%BE%AE%E4%BF%A1%E5%85%AC%E4%BC%97%E5%8F%B7.jpg)

```text
我是「猿码天地」，一个热爱技术、热爱编程的IT猿。技术是开源的，知识是共享的！
写作是对自己学习的总结和记录，如果您对 Java、分布式、微服务、中间件、Spring Boot、Spring Cloud等技术感兴趣，可以关注我的动态，我们一起学习，一起成长！
用知识改变命运，让家人过上更好的生活，互联网人一家亲！
```

#### 项目介绍
dubbo 组件学习，完整的项目和详细的操作步骤

#### 软件架构
dubbo-demo-api API  
dubbo-demo-consumer 服务消费者  
dubbo-demo-parent 父模块  
dubbo-demo-provider-20880 服务提供者 端口 20880  
dubbo-demo-provider-20881 服务提供者 端口 20881  

#### 模块说明

1. 安装Dubbo服务注册中心（zookeeper）
下载 http://mirror.bit.edu.cn/apache/zookeeper/zookeeper-3.4.12/ 解压，将文件放到磁盘，修改config下的zoo_sample.cfg为zoo.cfg，启动bin下的zkServer.cmd即可。
2. 发布Dubbo服务
下载Demo https://github.com/alibaba/dubbo 参考dubbo-demo-provider-20880
3. Dubbo Admin管理控制台
将dubbo-admin-2.6.0.war包解压后放到tomcat ROOT目录，先启动注册中心zookeeper服务，再启动tomcat，启动成功后访问：http://localhost:8080/  用户名/密码：root/root
4. 消费Dubbo服务
远程调用服务 参考 dubbo-demo-consumer 用法：先启动zookeeper服务注册中心，再启动tomcat dubbo admin管理控制台，然后启动
服务提供者dubbo-demo-provider-20880（测试类），在管理控制台查看是否启动成功，启动成功后再启动服务消费者dubbo-demo-consumer（测试类）
5. 接口抽取及依赖版本统一
新建dubbo-demo-api 抽取ProviderService接口/新建dubbo-demo-parent实现依赖版本统一管理。
6. Dubbo服务集群实现负载均衡
新建dubbo-demo-provider-20881 复制dubbo-demo-provider-20880工程 改下端口就行。

dubbo-demo-provider.xml文件
~~~
<!-- 使用dubbo协议，在20881端口暴露服务 -->
<dubbo:protocol name="dubbo" port="20881"/>
~~~
ProviderServiceImpl.java文件
~~~
public class ProviderServiceImpl implements ProviderService {
    public String sayHello(String name) {
        return "服务001(端口:20881)"+name;
    }
}
~~~
ProviderTest20881.java文件
~~~
public class ProviderTest20881 {
   public static void main(String[] args) {
      ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"dubbo-demo-provider.xml"});
        context.start();
        System.out.println("服务提供者向zookeeper注册中心注册服务成功（端口：20881）");
        try {
         System.in.read();
      } catch (IOException e) {
         e.printStackTrace();
      }
        context.close();
   }
}
~~~
用法：先启动zookeeper服务注册中心，再启动tomcat dubbo admin管理控制台，然后启动服务提供者dubbo-demo-provider-20880（测试类）
和dubbo-demo-provider-20881（测试类），在管理控制台查看是否启动成功，启动成功后再启动服务消费者dubbo-demo-consumer（测试类），在控制台查看每次消费者调用哪一个提供者，达到负载均衡的目的。

```text
你多学一样本事，就少说一句求人的话，现在的努力，是为了以后的不求别人，实力是最强的底气。记住，活着不是靠泪水博得同情，而是靠汗水赢得掌声。
```

### [猿码天地-Java知识学堂脑图](https://www.processon.com/view/link/6035ed1f079129248a64a6af) （查看文件密码：请关注公众号【猿码天地】，回复关键字‘活到老学到老’获取）
### [猿码天地-Java超神之路脑图](https://www.processon.com/view/link/6035f068e0b34d124437e0e1) （查看文件密码：请关注公众号【猿码天地】，回复关键字‘活到老学到老’获取）