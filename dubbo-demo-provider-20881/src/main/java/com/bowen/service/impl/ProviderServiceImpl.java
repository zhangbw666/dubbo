package com.bowen.service.impl;

import com.bowen.service.ProviderService;

/**
 * Created by zhangbw on 2019/1/23
 * Describe：服务提供者接口实现类
 */
public class ProviderServiceImpl implements ProviderService {

    public String sayHello(String name) {
        return "服务001(端口:20881)"+name;
    }
}
